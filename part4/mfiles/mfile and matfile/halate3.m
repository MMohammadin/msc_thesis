load mpc
DG=1.8;
mpc.bus(18,3)=mpc.bus(18,3)-DG
mpopt = mpoption('VERBOSE',0,'OUT_SYS_SUM',0,'OUT_BUS' ,0,'OUT_BRANCH',0,'ENFORCE_Q_LIMS',3);
results=runpf(mpc,mpopt);    %pfmax_all=result.pfmax_all;%     if (strcmp(result.problem,'nothing'))==1%               if result.pfmax_all<14%             dgmax=-(mpc.bus(13,3))+lob14;%             %                             qc=result.qc;                        %                             vr_step=result.vr_step;                       %                             oltc_step=result.oltc_step;                       %                             loss=result.loss;                       %                             ii;                        %                             Vmax_all=[Vmax_all vmax_all];Vr_step=[Vr_step vr_step];Oltc_step=[Oltc_step oltc_step];Loss=[Loss loss];II=[II ii];Vmin_all=[Vmin_all vmin_all];Qc=[Qc qc];                        %                             save (bus19result,'Vmax_all','II','Vmin_all','Qc','Vr_step','Oltc_step','Loss');%         end%         %     else; can_dg_increase=0;need_vc=0;%     end
VM=results.bus(:,8);
loss=1000*sum(abs(abs(results.branch(:,14))-abs(results.branch(:,16))));
x=ones(1,32);
x=[x 0 0 0 0 0];
[~, SortOrder]=sort(x);
close_branch=SortOrder(6:37);
 a=mpc.branch;
t=a(close_branch,[1 2 3 4]);
v=VM;
min=min(v)
% function ldist=sag_calculate(v,t)
% load v
zdata33 = [ 18     0     0.00    0.2
    1	     3   	0 	  0
    t   ];
Zbus=zbuild(zdata33);
Zf=0;
vf7=zeros(1,33);vf8=zeros(1,33);vf24=zeros(1,33);vf25=zeros(1,33);vf32=zeros(1,33);
for nf=1:33;                %     for i=1:33  %         vf(i,nf)=v(i)-v(nf)*Zbus(i,nf)/(Zf + Zbus(nf,nf));
    vf7(nf) = v(7) - v(nf)*Zbus(7,nf)/(Zf + Zbus(nf,nf));
    vf8(nf) = v(8) - v(nf)*Zbus(8,nf)/(Zf + Zbus(nf,nf));
    vf24(nf) = v(24) - v(nf)*Zbus(24,nf)/(Zf + Zbus(nf,nf));
    vf25(nf) = v(25) - v(nf)*Zbus(25,nf)/(Zf + Zbus(nf,nf));
    vf32(nf) = v(32) - v(nf)*Zbus(32,nf)/(Zf + Zbus(nf,nf));   %     end
end
% vf=abs(vf);
% ldist=sum(sum(vf));
vf7=abs(vf7);vf8=abs(vf8);vf24=abs(vf24);vf25=abs(vf25);vf32=abs(vf32);
a= vf24<(0.4*ones(1,33));b= vf25<(0.2*ones(1,33));
c=vf7<(0.4*ones(1,33));d=vf8<(0.4*ones(1,33));
e=vf32<(0.4*ones(1,33));
ldist=sum(a)+sum(b)+sum(c)+sum(d)+sum(e);
ldist
loss
min=min(v)
