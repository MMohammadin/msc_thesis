%% Start of Program
function [gbcostmat,gbposition]=mainbpso3(DG,np,MaxIt)
% tic
clc;
%% Problem Params
nbus=33;
% nbus=input('Enter a number of nodes of network');
switch  nbus;case 19;nvar=13;%tie=3;
    case 33;nvar=35;%tie=5;
end
    %% Algorithm Parameters
% DG=2 ; np =20; MaxIt =200
    C1 = 2;C2 =4-C1;w=1.2;wdamp= (1/6)^(1/MaxIt);
    %% initial swarm(genrate particle positions or configurations which r radial)
    %preallocation
    particle(1,np).position = [];particle(1,np).Vel = [];particle(1,np).Cost = [];particle(1,np).LBposition = [];particle(1,np).LBCost = [];particle(1,np).Vel = zeros(np,nvar);particle(1,np).Cost = zeros(1,nvar);particle(1,np).LBposition = zeros(np,nvar);particle(1,np).LBCost = zeros(1,nvar);gbposition = round(rand(1,nvar));
    GBCost = inf;gbcostmat =zeros(1,MaxIt);
    for ii = 1:np
        S='go';
        while(strcmp(S,'go'))==1
            particle(ii).position=round(rand(1,nvar));
            particle(ii).position = set_position(particle(ii).position,nbus);
            [r,~]=radialcheck33(particle(ii).position);
            if r==1
                [~,ldist]=Mycost33(particle(ii).position,DG);        %             loss=(Mycost33(particle(ii).position));
                 S='stop';
                particle(ii).Vel = rand(1,nvar);
                particle(ii).Cost =ldist;
                particle(ii).LBposition = particle(ii).position;
                particle(ii).LBCost = particle(ii).Cost;
                if particle(ii).LBCost < GBCost;
                    gbposition = particle(ii).LBposition;
                    GBCost = particle(ii).LBCost;
                end
            end
        end
    end
    gbcostmat(1,1)=159;gbcostmat(1,2)=GBCost;
    for Iter= 3:MaxIt
        %% Velocity Update
        for ii = 1:np
            particle(ii).Vel =w*particle(ii).Vel+C1*rand*(particle(ii).LBposition - particle(ii).position) + C2*rand*(gbposition - particle(ii).position);
            %% position update
            particle(ii).position=set_position(particle(ii).Vel,nbus);
            [~,ldist]=Mycost33(particle(ii).position,DG);
            particle(ii).Cost = ldist;
            %% GB and LB Update
            if particle(ii).Cost < particle(ii).LBCost
                particle(ii).LBposition = particle(ii).position;
                particle(ii).LBCost = particle(ii).Cost;
                if particle(ii).LBCost < GBCost;
                    gbposition = particle(ii).LBposition;
                    GBCost = particle(ii).LBCost;
                end
            end
        end
        %% Some Plots
        w=w*wdamp;
        gbcostmat(1,Iter) = GBCost;
        %        for u=1:50; n(u,1)=particle(u).Cost; end; n'
        % [Iter GBCost];
    end
%     figure;
%     plot(gbcostmat,'LineWidth',0.5,'Color',[0 0 0]);
%     grid
%     xlabel('Iteration Number');
%     ylabel('ndist');
%     title('33-Bus System-mbpso');
%     gb=GBCost;
    
