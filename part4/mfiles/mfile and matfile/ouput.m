clc
clear
close all
DGC=[0 .5  1  1.5  2  2.5  3  3.5  4];
load meangndist meangndist
load meanloss meanloss
result=[meangndist',meanloss'];
bar(DGC,result,1);
legend('Ndist','Loss')
grid
meangndist
meangndist=meangndist(2:end)
figure
result=[meangndist'];
DGC=[.5  1  1.5  2  2.5  3  3.5  4];
bar(DGC,result,0.5);



load y3
gbcostmat=y(3,1:200);
figure;
plot(gbcostmat,'LineWidth',0.5,'Color',[0 0 0]);
grid
xlabel('Iteration Number');
ylabel('ndist');
title('33-Bus System-mbpso')

b=y(3,201:235);
x=[1 b(1:9) 1 b(10:35)];
DG=3;
load mpc %mpc is case 33
mpc.bus(18,3)=mpc.bus(18,3)-DG ;%  3=collnm of PD
mpc.branch(:,11)=(x)';                                                    %while can_dg_increase    %     increase_counter=increase_counter+1; help=mpc.bus(13,PD)-dgstep;%%     mpc.bus(13,PD)=mpc.bus(13,PD)-dgstep;          % mpc.bus(13,3)=help;%     help=help -dgstep;  (mpc.bus(13,3))=help;   % (mpc.bus(13,3))-dgstep;%     % mpc.gen(2,2)=dg; ii=-1; for i=1:30  %         for h=1:24%             mpc.bus(:,PD)=ppd(:,h,i);mpc.bus(:,QD)=pqd(:,h,i);%             mpc.bus(13,PD)=help;%             need_vc=1;%             while  need_vc==1;ii=ii+1;    % [mpc_vc33 controller_value]=v33(mpc);                                                     %Vm=result.Vm;vmin_all=min(Vm);
mpopt = mpoption('VERBOSE',0,'OUT_SYS_SUM',0,'OUT_BUS' ,0,'OUT_BRANCH',0,'ENFORCE_Q_LIMS',3);
results=runpf(mpc,mpopt);    %pfmax_all=result.pfmax_all;%     if (strcmp(result.problem,'nothing'))==1%  


define_constants
pf=results.branch(:,PF);
pf=abs(pf);
pf_after=pf;
load mpc
mpopt = mpoption('VERBOSE',0,'OUT_SYS_SUM',0,'OUT_BUS' ,0,'OUT_BRANCH',0,'ENFORCE_Q_LIMS',3);
results=runpf(mpc,mpopt);    %pfmax_all=result.pfmax_all;%     if (strcmp(result.problem,'nothing'))==1%               if result.pfmax_all<14%             dgmax=-(mpc.bus(13,3))+lob14;%             %                             qc=result.qc;                        %                             vr_step=result.vr_step;                       %                             oltc_step=result.oltc_step;                       %                             loss=result.loss;                       %                             ii;                        %                             Vmax_all=[Vmax_all vmax_all];Vr_step=[Vr_step vr_step];Oltc_step=[Oltc_step oltc_step];Loss=[Loss loss];II=[II ii];Vmin_all=[Vmin_all vmin_all];Qc=[Qc qc];                        %                             save (bus19result,'Vmax_all','II','Vmin_all','Qc','Vr_step','Oltc_step','Loss');%         end%         %     else; can_dg_increase=0;need_vc=0;%     end
pf=results.branch(:,PF);
pf_before=pf;
m=[pf_after,pf_before];
figure
bar(m)


b=y(3,201:235);
x=[1 b(1:9) 1 b(10:35)];
DG=3
load mpc %mpc is case 33
mpc.bus(18,3)=mpc.bus(18,3)-DG ;%  3=collnm of PD
mpc.branch(:,11)=(x)';                                                    %while can_dg_increase    %     increase_counter=increase_counter+1; help=mpc.bus(13,PD)-dgstep;%%     mpc.bus(13,PD)=mpc.bus(13,PD)-dgstep;          % mpc.bus(13,3)=help;%     help=help -dgstep;  (mpc.bus(13,3))=help;   % (mpc.bus(13,3))-dgstep;%     % mpc.gen(2,2)=dg; ii=-1; for i=1:30  %         for h=1:24%             mpc.bus(:,PD)=ppd(:,h,i);mpc.bus(:,QD)=pqd(:,h,i);%             mpc.bus(13,PD)=help;%             need_vc=1;%             while  need_vc==1;ii=ii+1;    % [mpc_vc33 controller_value]=v33(mpc);                                                     %Vm=result.Vm;vmin_all=min(Vm);
mpopt = mpoption('VERBOSE',0,'OUT_SYS_SUM',0,'OUT_BUS' ,0,'OUT_BRANCH',0,'ENFORCE_Q_LIMS',3);
results=runpf(mpc,mpopt);    %pfmax_all=result.pfmax_all;%     if (strcmp(result.problem,'nothing'))==1%               if result.pfmax_all<14%             dgmax=-(mpc.bus(13,3))+lob14;%             %                             qc=result.qc;                        %                             vr_step=result.vr_step;                       %                             oltc_step=result.oltc_step;                       %                             loss=result.loss;                       %                             ii;                        %                             Vmax_all=[Vmax_all vmax_all];Vr_step=[Vr_step vr_step];Oltc_step=[Oltc_step oltc_step];Loss=[Loss loss];II=[II ii];Vmin_all=[Vmin_all vmin_all];Qc=[Qc qc];                        %                             save (bus19result,'Vmax_all','II','Vmin_all','Qc','Vr_step','Oltc_step','Loss');%         end%         %     else; can_dg_increase=0;need_vc=0;%     end
VM=results.bus(:,8);
for i=1:33
bus(i)=i
end
figure
plot(bus,VM,'-O')
load v
hold on;plot(bus,v,'--O')




