clc
clear
load y3
b=y(3,201:235);
x=[1 b(1:9) 1 b(10:35)];
[~, SortOrder]=sort(x);
open_branch=SortOrder(1:5)


load mpc
DG=3
mpc.bus(18,3)=mpc.bus(18,3)-DG ;%  3=collnm of PD
mpc.branch(:,11)=(x)';                                                    %while can_dg_increase    %     increase_counter=increase_counter+1; help=mpc.bus(13,PD)-dgstep;%%     mpc.bus(13,PD)=mpc.bus(13,PD)-dgstep;          % mpc.bus(13,3)=help;%     help=help -dgstep;  (mpc.bus(13,3))=help;   % (mpc.bus(13,3))-dgstep;%     % mpc.gen(2,2)=dg; ii=-1; for i=1:30  %         for h=1:24%             mpc.bus(:,PD)=ppd(:,h,i);mpc.bus(:,QD)=pqd(:,h,i);%             mpc.bus(13,PD)=help;%             need_vc=1;%             while  need_vc==1;ii=ii+1;    % [mpc_vc33 controller_value]=v33(mpc);                                                     %Vm=result.Vm;vmin_all=min(Vm);
%% information need for zdata
a=mpc.branch;
[~, SortOrder]=sort(x);
close_branch=SortOrder(6:37);
t=a(close_branch,[1 2 3 4]);
mpopt = mpoption('VERBOSE',0,'OUT_SYS_SUM',0,'OUT_BUS' ,0,'OUT_BRANCH',0,'ENFORCE_Q_LIMS',3);
results=runpf(mpc,mpopt);    %pfmax_all=result.pfmax_all;%     if (strcmp(result.problem,'nothing'))==1%               if result.pfmax_all<14%             dgmax=-(mpc.bus(13,3))+lob14;%             %                             qc=result.qc;                        %                             vr_step=result.vr_step;                       %                             oltc_step=result.oltc_step;                       %                             loss=result.loss;                       %                             ii;                        %                             Vmax_all=[Vmax_all vmax_all];Vr_step=[Vr_step vr_step];Oltc_step=[Oltc_step oltc_step];Loss=[Loss loss];II=[II ii];Vmin_all=[Vmin_all vmin_all];Qc=[Qc qc];                        %                             save (bus19result,'Vmax_all','II','Vmin_all','Qc','Vr_step','Oltc_step','Loss');%         end%         %     else; can_dg_increase=0;need_vc=0;%     end
loss=1000*sum(abs(abs(results.branch(:,14))-abs(results.branch(:,16))))
VM=results.bus(:,8)
min=min(VM)
ldist=sag_calculate(VM,t)