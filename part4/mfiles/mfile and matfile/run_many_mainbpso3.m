clc
tic
clear;close all
DG=0;np=5;MaxIt=2;timeofrun=5;
help=MaxIt+35;
y=zeros(timeofrun,help);
for i=1:timeofrun
[gbcostmat gbposition]=mainbpso3(DG,np,MaxIt);                       
% [loss open_branch]=loss_open_branch(gbposition,DG);
y(i,:)=[gbcostmat gbposition ];                              % each page is parfor one run 
end
save test.mat y;
toc

tic
clear;close all
DG=.5;np=50;MaxIt=200;timeofrun=250;
help=MaxIt+35;
y=zeros(timeofrun,help);
parfor i=1:timeofrun
[gbcostmat gbposition]=mainbpso3(DG,np,MaxIt);                       % loss=gbcostmat(end);% save result_parfor_dg4.1  controller_value loss gbposition
[loss open_branch]=loss_open_branch(b,DG);y(i,:)=[gbcostmat gbposition open_branch loss];                              % each page is parfor one run 
end
save ynim.mat y;
toc

tic
clear;close all
DG=1;np=50;MaxIt=200;timeofrun=250;
help=MaxIt+35;
y=zeros(timeofrun,help);
parfor i=1:timeofrun
[gbcostmat gbposition]=mainbpso3(DG,np,MaxIt);                       % loss=gbcostmat(end);% save result_parfor_dg4.1  controller_value loss gbposition
[loss open_branch]=loss_open_branch(b,DG);y(i,:)=[gbcostmat gbposition open_branch loss];                              % each page is parfor one run 
end
save y1.mat y;
toc

tic
clear;close all
DG=1.5;np=50;MaxIt=200;timeofrun=250;
help=MaxIt+35;
y=zeros(timeofrun,help);
parfor i=1:timeofrun
[gbcostmat gbposition]=mainbpso3(DG,np,MaxIt);                       % loss=gbcostmat(end);% save result_parfor_dg4.1  controller_value loss gbposition
[loss open_branch]=loss_open_branch(b,DG);y(i,:)=[gbcostmat gbposition open_branch loss];                              % each page is parfor one run 
end
save y1o5.mat y;
toc


tic
clear;close all
DG=2;np=50;MaxIt=200;timeofrun=250;
help=MaxIt+35;
y=zeros(timeofrun,help);
parfor i=1:timeofrun
[gbcostmat gbposition]=mainbpso3(DG,np,MaxIt);                       % loss=gbcostmat(end);% save result_parfor_dg4.1  controller_value loss gbposition
[loss open_branch]=loss_open_branch(b,DG);y(i,:)=[gbcostmat gbposition open_branch loss];                              % each page is parfor one run 
end
save y2.mat y;
toc

5
tic
clear;close all
DG=2.5;np=50;MaxIt=200;timeofrun=250;
help=MaxIt+35;
y=zeros(timeofrun,help);
parfor i=1:timeofrun
[gbcostmat gbposition]=mainbpso3(DG,np,MaxIt);                       % loss=gbcostmat(end);% save result_parfor_dg4.1  controller_value loss gbposition
[loss open_branch]=loss_open_branch(b,DG);y(i,:)=[gbcostmat gbposition open_branch loss];                              % each page is parfor one run 
end
save y2o5.mat y;
toc

tic
clear;close all
DG=3;np=50;MaxIt=200;timeofrun=250;
help=MaxIt+35;
y=zeros(timeofrun,help);
parfor i=1:timeofrun
[gbcostmat gbposition]=mainbpso3(DG,np,MaxIt);                       % loss=gbcostmat(end);% save result_parfor_dg4.1  controller_value loss gbposition
[loss open_branch]=loss_open_branch(b,DG);y(i,:)=[gbcostmat gbposition open_branch loss];                              % each page is parfor one run 
end
save y3.mat y;
toc

tic
clear;close all
DG=3.5;np=50;MaxIt=200;timeofrun=250;
help=MaxIt+35;
y=zeros(timeofrun,help);
parfor i=1:timeofrun
[gbcostmat gbposition]=mainbpso3(DG,np,MaxIt);                       % loss=gbcostmat(end);% save result_parfor_dg4.1  controller_value loss gbposition
[loss open_branch]=loss_open_branch(b,DG);y(i,:)=[gbcostmat gbposition open_branch loss];                              % each page is parfor one run 
end
save y3o5.mat y;
toc

tic
clear;close all
DG=4;np=50;MaxIt=200;timeofrun=250;
help=MaxIt+35;
y=zeros(timeofrun,help);
parfor i=1:timeofrun
[gbcostmat gbposition]=mainbpso3(DG,np,MaxIt);                       % loss=gbcostmat(end);% save result_parfor_dg4.1  controller_value loss gbposition
[loss open_branch]=loss_open_branch(b,DG);y(i,:)=[gbcostmat gbposition open_branch loss];                              % each page is parfor one run 
end
save y4.mat y;
toc

