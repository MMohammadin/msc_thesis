function PlotSolution(sol,model)

    X=model.X;
    Y=model.Y;
    n=model.n;
    
    A=sol.A;
    
    for i=1:n
        for j=i+1:n
            if A(i,j)~=0
                plot([X(i) X(j)],[Y(i) Y(j)],'b','LineWidth',2);
                hold on;
            end
        end
    end
    plot(X,Y,'ko','MarkerSize',12,'MarkerFaceColor',[1 1 0]);
    hold off;

end