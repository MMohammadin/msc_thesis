function ldist=sag_calculate(v,t)
    % load v
zdata33 = [ 18     0     0.00    0.2
    1	     3   	0 	  0       
t   ]; 
Zbus=zbuild(zdata33);
Zf=0;
vf7=zeros(1,33);vf8=zeros(1,33);vf24=zeros(1,33);vf25=zeros(1,33);vf32=zeros(1,33);
for nf=1:33;                %     for i=1:33  %         vf(i,nf)=v(i)-v(nf)*Zbus(i,nf)/(Zf + Zbus(nf,nf));
vf7(nf) = v(7) - v(nf)*Zbus(7,nf)/(Zf + Zbus(nf,nf));
vf8(nf) = v(8) - v(nf)*Zbus(8,nf)/(Zf + Zbus(nf,nf));
vf24(nf) = v(24) - v(nf)*Zbus(24,nf)/(Zf + Zbus(nf,nf));
vf25(nf) = v(25) - v(nf)*Zbus(25,nf)/(Zf + Zbus(nf,nf));
vf32(nf) = v(32) - v(nf)*Zbus(32,nf)/(Zf + Zbus(nf,nf));   %     end
end
% vf=abs(vf);
% ldist=sum(sum(vf));
vf7=abs(vf7);vf8=abs(vf8);vf24=abs(vf24);vf25=abs(vf25);vf32=abs(vf32);
a= vf24<(0.4*ones(1,33));b= vf25<(0.2*ones(1,33));
c=vf7<(0.4*ones(1,33));d=vf8<(0.4*ones(1,33));
e=vf32<(0.4*ones(1,33));
ldist=sum(a)+sum(b)+sum(c)+sum(d)+sum(e);
%% the first part ke gavabash khob bood
% Vfm=symfault(zdata33,Zbus,v);
% sag=v-Vfm';
% a=sag'<(0.2*ones(1,33));
% ldist=sum(a);
