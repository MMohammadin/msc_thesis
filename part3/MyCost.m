function [dgmax,loss] =Mycost(b)
global nbus  x Vmin Vmax
define_constants;VR_Min=0.9;VR_Max=1.1;dgmax=1;baseMVA=100;can_dg_increase=1;increase_counter=0; Oltc_step=[];Loss=[];II=[];Vmax_all=[];Vmin_all=[];Vr_step=[];Qc=[];
load ppqqd.mat
r=radialcheck(b,nbus);
if r==1
switch  nbus;
case 19;bus19result ='bus19.mat';dgstep=1;
OLTC_Min=0.9;OLTC_Max=1.1;Shunt_Nominal=0.015;Shunt_Step=0.003;OLTC_step=0.00625;VR_step=0.0125;
mpc=loadcase('case1');lob14=mpc.bus(14,PD);mpc.branch(:,11)=(x)';
 
while can_dg_increase
    increase_counter=increase_counter+1;         % dg= -(mpc.bus(13,3))+mpc.bus(14,3) ;  dg=dg-dgstep;   (mpc.bus(13,3))=dg
    help=mpc.bus(13,PD)-dgstep;%             mpc.bus(13,PD)=mpc.bus(13,PD)-dgstep;          % mpc.bus(13,3)=help; help=help -dgstep;  (mpc.bus(13,3))=help;   % (mpc.bus(13,3))-dgstep;         % mpc.gen(2,2)=dg;
    ii=-1;
    for i=1:30
        for h=1:24
            mpc.bus(:,PD)=ppd(:,h,i);mpc.bus(:,QD)=pqd(:,h,i);
            mpc.bus(13,PD)=help;
            need_vc=1;
            while  need_vc==1;ii=ii+1;
                result=dlf(mpc);Vm=result.Vm;vmin_all=min(Vm);
                vmax_all=max(Vm); %pfmax_all=result.pfmax_all;
                if (strcmp(result.problem,'nothing'))==1
                    need_vc=0;
                    if result.pfmax_all<14
                        dgmax=-(mpc.bus(13,3))+lob14;
                        %                             qc=result.qc;                        %                             vr_step=result.vr_step;                       %                             oltc_step=result.oltc_step;                       %                             loss=result.loss;                       %                             ii;                        %                             Vmax_all=[Vmax_all vmax_all];Vr_step=[Vr_step vr_step];Oltc_step=[Oltc_step oltc_step];Loss=[Loss loss];II=[II ii];Vmin_all=[Vmin_all vmin_all];Qc=[Qc qc];                        %                             save (bus19result,'Vmax_all','II','Vmin_all','Qc','Vr_step','Oltc_step','Loss');
                    end
                elseif (vmin_all<Vmin)
                    if (mpc.bus(12,6)<baseMVA*Shunt_Nominal)&&(Vm(12)<=Vmax)
                        mpc.bus(12,6)=min(mpc.bus(12,6)+baseMVA*Shunt_Step,baseMVA*Shunt_Nominal);
                    elseif (mpc.branch(10,9)>VR_Min)&&(Vm(9)<=Vmax)%%             mpc.bus(12,6)=0;
                        mpc.branch(10,9)=max(mpc.branch(10,9)-VR_step,VR_Min);
                    elseif (mpc.branch(1,9)>OLTC_Min)&&(Vm(2)<=Vmax)%%             mpc.bus(12,6)=0;
                        mpc.branch(10,9)=1; mpc.branch(1,9)=max(mpc.branch(1,9)-OLTC_step,OLTC_Min);
                    else; can_dg_increase=0;need_vc=0;
                    end
                elseif(vmax_all>Vmax)
                    if (mpc.bus(12,6)>0)&&(Vm(12)>=Vmin)
                        mpc.bus(12,6)=max(mpc.bus(12,6)-baseMVA*Shunt_Step,0);
                    elseif (mpc.branch(10,9)<VR_Max)&&(Vm(9)>=Vmin)
                        mpc.bus(12,6)=baseMVA*Shunt_Nominal;mpc.branch(10,9)=min(mpc.branch(10,9)+VR_step,VR_Max);
                    elseif (mpc.branch(1,9)<OLTC_Max)&&(Vm(2)>=Vmin)
                        mpc.bus(12,6)=baseMVA*Shunt_Nominal;mpc.branch(10,9)=VR_Min;mpc.branch(1,9)=min(mpc.branch(1,9)+OLTC_step,OLTC_Max);
                    else; can_dg_increase=0;need_vc=0;
                    end
                end
                if ii>5000;need_vc=0;can_dg_increase=0;end
            end
        end
    end
end
case 33;f=4;
end
end
end











