

function NewChrIx = sus(FitnV,Nsel);

% Identify the population size (Nind)
   [Nind,ans] = size(FitnV);

% Perform stochastic universal sampling
   cumfit = cumsum(FitnV);
   trials = cumfit(Nind) / Nsel * (rand + (0:Nsel-1)');
   Mf = cumfit(:, ones(1, Nsel));
   Mt = trials(:, ones(1, Nind))';
   [NewChrIx, ans] = find(Mt < Mf & [ zeros(1, Nsel); Mf(1:Nind-1, :) ] <= Mt);

% Shuffle new population
   [ans, shuf] = sort(rand(Nsel, 1));
   NewChrIx = NewChrIx(shuf);


% This function performs selection with STOCHASTIC UNIVERSAL SAMPLING.
% 
% Syntax:  NewChrIx = sus(FitnV, Nsel)
% 
% Input parameters:
%    FitnV     - Column vector containing the fitness values of the
%                individuals in the population.
%    Nsel      - number of individuals to be selected
% 
% Output parameters:
%    NewChrIx  - column vector containing the indexes of the selected
%                individuals relative to the original population, shuffled.
%                The new population, ready for mating, can be obtained
%                by calculating OldChrom(NewChrIx,:).
% 
% Author:     Hartmut Pohlheim (Carlos Fonseca)
% History:    12.12.93     file created
%             22.02.94     clean up, comments

%% my program
% function NewChrIx = sus(p,n);
% n=11;
% p=[.1 .2 .3 .4];
% p=cumsum(p);
% SIZE=size(p,2)
% NewChrIx=zeros(1,SIZE)
% r = 1/n*rand;
% k=1;c=p(1);sigma_ni=0
% while sigma_ni<n
%     if r<=c
%         NewChrIx(k)=NewChrIx(k)+1;r=r+1/n;
%         sigma_ni=sigma_ni+1;
%     else
%         k=k+1;c=c+p(k)
%     end 
%     end
% end

