tic
clc;
clear;
close all;
%% Problem Definition
global nbus CostFunction nvar  VarMin VarMax  nPop ...
    MaxIt nEmp alpha beta pRevolution mu zeta tie 
nbus=19;
% nbus=input('Enter a number of buses of network');
switch nbus;case 19;nvar=13;tie=3;case 33;nvar=35;tie=5;end
CostFunction=@(xhat) MyCost(xhat);   % Cost Function
VarMin=.4166;                        % Lower Bound of Variables
VarMax=1;                            % Upper Bound of Variables
%% ICA Parameters
MaxIt=200;
nPop=3;nEmp=2;
alpha=1;
beta=1;
pRevolution=0.2; mu=0.05; zeta=0.1;
% ShareSettings;
%% Initialization
% Initialize Empires
emp=CreateInitialEmpires();
% Array to Hold Best Cost Values
BestCost=zeros(MaxIt,1);
gbcost = inf;gbcostmat =zeros(1,MaxIt);
%% ICA Main Loop
% while toc <12
for it=1:MaxIt
    if toc <60
        emp=AssimilateColonies(emp);
        emp=DoRevolution(emp);
        emp=IntraEmpireCompetition(emp);
        emp=UpdateTotalCost(emp);
        emp=InterEmpireCompetition(emp);
        imp=[emp.Imp];
        [~, BestImpIndex]=min([imp.Cost]);
        BestSol=imp(BestImpIndex);
        %     BestCost(it)=BestSol.Cost;
        %  BestCost=BestSol.Cost;
        if BestSol.Cost < gbcost
            gbcost=BestSol.Cost;
        end
        gbcostmat(1,it) = gbcost;
    end
    % gb=mean(gbcostmat)
end
%% Results
figure;
gb=mean(gbcostmat);
plot(gbcostmat,'LineWidth',0.5,'Color',[0 0 0]);
xlabel('Iteration');
ylabel('System Loss (kW)');
title('33-Bus System-mica');
% toc
