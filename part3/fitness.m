function fitness = dgcapacity(x)
bus19result ='bus19.mat';define_constants;load ppqqd.mat
% function fitness = dgcapacity(X)
 global nbus
 doxd=.001;DG2_Rating=8;QMax_DG2=4;QMin_DG2=-4;Vmax=1.05;Vmin=0.95;
 Shunt_Nominal=0.015; Shunt_Step=0.003;OLTC_Step=0.00625;
 n=1;Vup=1.025;dgmax=0;
 VR_Step=0.0125;VR_Min=0.9;VR_Max=1.1;OLTC_Min=0.9;OLTC_Max=1.1;
Case=loadcase('case1');bus=Case.bus;branch=Case.branch;gen=Case.gen;bus0=bus;gen0=gen;
gen(2,2)=8.2;gen(2,4)=QMax_DG2;gen(2,5)=QMin_DG2;bus0(3:end,PD)=2;bus0(3:end,QD)=1;
zdata=[0   13   0  .20;0   18   0  .20;0   3    0  .22;branch(1:21,1:4)];
branch(1,TAP)=1; branch(10,TAP)=1;bus(12,BS)=0;
need_vc=1;ii=0;
voltages='r ok & we dot need vc';power_flow='within limits';fault_current='within limits';can_dg_increase=1;need_vc=1;
for i=1:100
for h=1:24
   can_dg_increase=1;gen(2,2)=6;zdata(2,4)=.21;branch(1,TAP)=1; branch(10,TAP)=1;bus(12,BS)=0;ii=0;
        while can_dg_increase
        gen(2,2)=gen(2,2)+.2;zdata(2,4)=zdata(2,4)-doxd;need_vc=1;%        load particle
        while need_vc
              branch(:,11)=x;

%             branch(:,11)=[1  1   1   1   1   1   0   1   1   1   1   1   1   1   0  1   1   1   1   1  0];
            bus(:,PD)=ppd(:,h,i);bus(:,QD)=pqd(:,h,i);
            results=runpf(bus,branch,gen);bus=results.bus;branch=results.branch;gen=results.gen;
            [baseMVA,bus,gen,branch]=runpf(bus,branch,gen);
            ii=ii+1; Vm=bus(:,8);Vmin_all=min(Vm);Vmax_all=max(Vm);
            if (Vmin_all>=Vmin)&& (Vmax_all<=Vmax)
                dg13=gen(2,2); need_vc=0;voltages='r ok & we dot need vc';Vmax_all=max(Vm);
                if abs(results.branch(13, PF))<10
                    power_flow='is within limits';pf_13=abs(results.branch(13, PF));
                end
                Zbus=zbuild(zdata);nf =6;Zf = 0;z_data(2,4)=zdata(2,4);If = Vm(nf)/(Zf + Zbus(nf, nf));Ifm = abs(If);
                QC=bus(12,BS);
                save (bus19result,'dg13','Vmax_all','pf_13','Ifm','i','h','Vmin_all','QC');
                if Ifm <8
                    can_dg_increase=1;
                else
                    can_dg_increase=0;
                end
            elseif (Vmin_all<Vmin)
                if (bus(12,6)<baseMVA*Shunt_Nominal)&&(Vm(12)<=Vmax)
                    bus(12,6)=min(bus(12,6)+baseMVA*Shunt_Step,baseMVA*Shunt_Nominal);
                elseif (branch(10,9)>VR_Min)&&(Vm(9)<=Vmax)
                    bus(12,6)=0; branch(10,9)=max(branch(10,9)-VR_Step,VR_Min);ii=ii+1;
                elseif (branch(1,9)>OLTC_Min)&&(Vm(2)<=Vmax)
                    bus(12,6)=0;branch(10,9)=1;branch(1,9)=max(branch(1,9)-OLTC_Step,OLTC_Min);
                else
                    need_vc=0;voltages='r not ok but we cant do vc';
                end
            elseif(Vmax_all>Vmax)
                if (bus(12,6)>0)&&(Vm(12)>=Vmin)
                    bus(12,6)=max(bus(12,6)-baseMVA*Shunt_Step,0);
                elseif (branch(10,9)<VR_Max)&&(Vm(9)>=Vmin)
                    bus(12,6)=baseMVA*Shunt_Nominal;branch(10,9)=min(branch(10,9)+VR_Step,VR_Max);
                elseif (branch(1,9)<OLTC_Max)&&(Vm(2)>=Vmin)
                    bus(12,6)=baseMVA*Shunt_Nominal;branch(10,9)=VR_Min;branch(1,9)=min(branch(1,9)+OLTC_Step,OLTC_Max);
                else
                    need_vc=0;voltages='r not ok but we cant do vc';
                end
            end
         if ii>=2000
                need_vc=0;   
         end
        end
        end
load bus19.mat
fprintf('\n  i=%d h=%d dg13=%d Vmin_all=%d Ifm=%d Power folw of branch13=%d  QC=%d  OLTC TAP=%d VR TAP=%d\n',i,h,dg13,Vmin_all,Ifm,pf_13,QC,branch([1 10],TAP));
if dg13>dgmax
    dgmax=dg13;
end
    end
end
fitness=dgmax;
 end






























