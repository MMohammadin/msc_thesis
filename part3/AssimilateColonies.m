function emp=AssimilateColonies(emp)
%     global ProblemSettings; CostFunction=ProblemSettings.CostFunction;
%     VarSize=ProblemSettings.VarSize; VarMin=ProblemSettings.VarMin;
%     VarMax=ProblemSettings.VarMax; tie=ProblemSettings.tie;
%     global ICASettings; beta=ICASettings.beta; nvar=ProblemSettings.nvar;

global  CostFunction nvar  VarMin VarMax  nEmp  beta tie
nEmp=numel(emp);


for k=1:nEmp
    for i=1:emp(k).nCol
        
        emp(k).Col(i).Position = emp(k).Col(i).Position ...
            + beta*rand(1,nvar).*(emp(k).Imp.Position-emp(k).Col(i).Position);
        
        emp(k).Col(i).Position = max(emp(k).Col(i).Position,VarMin);
        emp(k).Col(i).Position = min(emp(k).Col(i).Position,VarMax);
        emp(k).Col(i).Position=double((emp(k).Col(i).Position)>=0.5);
        % modified ica
        Sigy = 1./(1+exp(-(emp(k).Col(i).Position)));
        rid=Sigy-rand(1,nvar);sortrid=sort(rid);
        sortridd(1:nvar)=sortrid(tie);
        emp(k).Col(i).Position = (rid > sortridd);
        %         [z,sol] =MyCost(country(i).Position);
        [emp(k).Col(i).Cost ] = CostFunction(emp(k).Col(i).Position);
%         emp(k).Col(i).Sol
    end
end

end