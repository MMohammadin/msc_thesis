clc
clear
 define_constants;
load result_for_dg4.1  controller_value loss gbposition
b=gbposition;
x=[1 b(1:9) 1 b(10:35)];
 load mpc
mpc.branch(:,11)=x;
mpc.bus(18,3)=mpc.bus(18,3)-4.1;
mpc.bus(14,BS)=controller_value(1)
mpc.bus(21,BS)=controller_value(2);
mpc.bus(23,BS)=controller_value(3);
mpc.branch(11,TAP)=controller_value(4);
runpf(mpc)
loss
