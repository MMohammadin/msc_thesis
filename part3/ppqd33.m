clc
clear all
ALCrVar=0.05;RLCrVar=0.05;
 ltr=[0.78 0.74 0.72 0.69 0.67 0.65 0.63 0.65 0.83 0.86 0.88 0.92 0.89 0.98 1.00 0.97 0.93 0.92 0.93 0.94 0.93 0.88 0.84 0.80];
define_constants

 for i=1:100
for h=1:24  
   Case=loadcase('case33');
   bus0=Case.bus;
   bus0(:,PD)=bus0(:,PD)*ltr(h)+ALCrVar*randn;
   bus0(:,QD)=(bus0(:,QD)*ltr(h))+RLCrVar*randn;
   ppd(:,h,i)=bus0(:,PD);
   pqd(:,h,i)=bus0(:,QD);

    
 end
 end
ppqqd33 ='ppqqd33.mat';
save (ppqqd33,'ppd','pqd');      
