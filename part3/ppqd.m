clc
clear all
pl=2;ql=1;DG1_Rating=8;ALCiVar=0.05;ALCcVar=0.1;ALCrVar=0.15;RLCiVar=0.005;RLCcVar=0.001;RLCrVar=0.0005;dgv=.15;bmax=19;QMax_DG2=4;QMin_DG2=-4;
DG1_Pat1=[0.5 0.55 0.4 0 0 0 0  0.5 0.7 0.91 0.95 1 0.95 0.9 0.9 0.85 0.7 0.6 0.65 0.7 0.75 0.7 0.6 0.5]; 
DG1_Pat2=[0.6 0.65 0.75 0.91 0.95 1 0.95 0.9 0.9 0.85 0.7 0.6 0.5 0.4 0.31 0.3 0.35 0.31 0.21 0 0 0 0.4 0.55];
DG1_Pat3=[0.2 0.21 0.25 0.3 0.31 0.35 0.4 0.42 0.5 0.51 0.55 0.58 0.6 0.55 0.51 0.49 0.45 0.43 0.4 0.35 0.3 0.27 0.23 0.2];
lti=[0.78 0.74 0.72 0.69 0.67 0.65 0.63 0.65 0.83 0.86 0.88 0.92 0.89 0.98 1.00 0.97 0.93 0.92 0.93 0.94 0.93 0.88 0.84 0.80];
ltc=[0.67 0.61 0.59 0.57 0.55 0.53 0.51 0.48 0.57 0.60 0.61 0.63 0.63 0.69 0.70 0.68 0.67 0.67 0.69 0.71 0.71 0.71 0.70 0.69]; 
ltr=[0.31 0.28 0.27 0.26 0.24 0.22 0.21 0.17 0.14 0.13 0.14 0.14 0.21 0.22 0.23 0.23 0.22 0.27 0.29 0.33 0.32 0.32 0.32 0.31];
fprintf('\n - - - - - - - - - - - - - - - - - \n')
DG1_patt=input('Enter the Pattern for DG1 Generation!!  1 , 2 or 3       ');
if DG1_patt==1
    DG1_Pat=DG1_Pat1;
elseif DG1_patt==2
    DG1_Pat=DG1_Pat2;
elseif DG1_patt==3
    DG1_Pat=DG1_Pat3;
end  
define_constants
ppd=zeros(bmax,24,100);pqd=zeros(bmax,24,100);
for i=1:100
for h=1:24   
Case=loadcase('case1');bus0=Case.bus;bus0(3:end,PD)=pl;bus0(3:end,QD)=ql;
    for N=3:7
            bus0(N,PD)=(bus0(N,PD)*ltr(h))+ALCrVar*randn;   bus0(N,QD)=(bus0(N,QD)*ltr(h))+RLCrVar*randn;
        end
        for N=[8 9 10 11 12 13 17]
            bus0(N,PD)=(bus0(N,PD)*lti(h))+ALCiVar*randn;   bus0(N,QD)=(bus0(N,QD)*lti(h))+RLCiVar*randn;
        end
        for N=[14 15 16 18 19]
            bus0(N,PD)=(bus0(N,PD)*ltc(h))+ALCcVar*randn;   bus0(N,QD)=(bus0(N,QD)*ltc(h))+RLCrVar*randn;
        end
            bus0(18,PD)=(-DG1_Pat(h)*DG1_Rating)+dgv*(-DG1_Pat(h)*DG1_Rating)+bus0(18,PD);
            ppd(:,h,i)=bus0(:,PD);
            pqd(:,h,i)=bus0(:,QD);
            end
end
ppqqd ='ppqqd.mat';
save (ppqqd,'ppd','pqd');      
% Case=loadcase('case1');bus=Case.bus;branch=Case.branch;gen=Case.gen;bus0=bus;gen0=gen;gen(2,2)=8.2;gen(2,4)=QMax_DG2;gen(2,5)=QMin_DG2;
% bus(:,PD)=ppd(:,1,1);bus(:,QD)=pqd(:,1,1);
% [baseMVA,bus,gen,branch]=runpf(bus,branch,gen);
% Vm=bus(:,8);Vmin_all=min(Vm);Vmax_all=max(Vm);
% f=2;   
% v ='v.mat';
% save (v,'Vm'); 
% p=2