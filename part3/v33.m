function [mpc_vc33 controller_value]=vc33(mpc)
% global nvar nbus tie x
define_constants;
% mpc=loadcase(case33);mpc.bus(14,BS)=.5;mpc.bus(21,BS)=.75;mpc.bus(23,BS)=1.5;mpc.bus(18,PD)=mpc.bus(18,PD)-4 ;
% load mpc
i=0;define_constants;Vvr_min=0.975;Vvr_max=1.025;baseMVA=100;cmax=1.5;Shunt_Step=0.3;VR_step=0.0125;vr_min=0.9;vr_max=1.1;%OLTC_step=0.00BS25;OLTC_Min=0.9;OLTC_Max=1.1;
mpopt = mpoption('VERBOSE',0,'OUT_SYS_SUM',0,'OUT_BUS' ,0,'OUT_BRANCH',0,'ENFORCE_Q_LIMS',3);
need_vc=1;c1=0;c2=0;c3=0;vrtap=1;
while need_vc
    mpc.bus(14,BS)=c1;mpc.bus(21,BS)=c2;mpc.bus(23,BS)=c3;mpc.branch(11,TAP)=vrtap;
    results=runpf(mpc,mpopt);
    c1=(mpc.bus(14,BS));c2=mpc.bus(21,BS);c3=mpc.bus(23,BS);vrtap=mpc.branch(11,TAP);
    vc1=results.bus(14,VM);vc2=(results.bus(21,VM));vc3=(results.bus(23,VM));Vvr=(results.bus(12,VM));
   controller_value=[c1 c2 c3 vrtap];
   if (Vvr_min<vc1&&vc1<Vvr_max)||(vc1<=Vvr_min&&c1==cmax)||(vc1>=Vvr_max&&c1==0)
       if (Vvr_min<vc2&&vc2<Vvr_max)||(vc2<=Vvr_min&&c2==cmax)||(vc2>=Vvr_max&&c2==0)
           if (Vvr_min<vc3&&vc3<Vvr_max)||(vc3<=Vvr_min&&c3==cmax)||(vc3>=Vvr_max&&c3==0)
               if(Vvr_min<Vvr<Vvr_max)||(Vvr<=Vvr_min&&vrtap==vr_max)||(vc3>=Vvr_max&&vrtap==vr_min)
                   need_vc=0;mpc_vc33=mpc;   controller_value=[c1 c2 c3 vrtap];
               elseif   Vvr<=Vvr_min
                   vrtap=vrtap-VR_step;i=i+1;
               else vc1>=Vvr_max;
                   vrtap=vrtap+VR_step;i=i+1;
               end
           elseif  vc3<=Vvr_min
               c3=c3+Shunt_Step;i=i+1;
           else vc3>=Vvr_max;
               c3=c3-Shunt_Step;i=i+1;
           end
       elseif   vc2<=Vvr_min
           c2=c2+Shunt_Step;i=i+1;
       else vc2>=Vvr_max;
           c2=c2-Shunt_Step;i=i+1;
       end
   elseif   vc1<=Vvr_min
       c1=c1+Shunt_Step;i=i+1;
   else vc1>=Vvr_max;
       c1=c1-Shunt_Step;i=i+1;
   end
    if i>40
        controller_value=[c1 c2 c3 vrtap];
        mpc_vc33=mpc;break
    end
end



