function result=dlf(mpc)
global result Vmin Vmax
Vmin=0.95;Vmax=1.05;        %maxpf=14;
define_constants;
mpopt = mpoption('VERBOSE',0,'OUT_SYS_SUM',0,'OUT_BUS' ,0,'OUT_BRANCH',0,'ENFORCE_Q_LIMS',3);
results=runpf(mpc,mpopt);
result.qc=results.bus(12,BS);result.vr_step=(mpc.branch(10,9));result.oltc_step=(mpc.branch(1,9));
Vm=results.bus(:,8);
result.Vm=Vm;
result.vmin_all=min(Vm);
result.Vmax_all=max(Vm);
result.pfmax_all=max(results.branch(:,PF));
result.loss=1000*sum(abs(abs(results.branch(:,14))-abs(results.branch(:,16))));
if result.vmin_all>Vmin && result.Vmax_all<Vmax 
    result.problem='nothing';
    
else
    result.problem='something';
    %     result.problem='Vmin';
% 
% 
% elseif result.Vmax_all>Vmax
%     result.problem='Vmax';
%     
% elseif result.pfmax_all>maxpf
%         result.problem='maxpf';
% 
% else
%     result.problem='nothing';

end
