clc;clear all;
% close all;
%% Problem Params
global nbus
nbus=33;
% nbus=input('Enter a number of nodes of network');
switch  nbus;case 19;nvar=13;tie=3;case 33;nvar=35;tie=5;end
%% Algorithm Parameters
np = 10;
% MaxIter =5000;C1 = 2;C2 = 4-C1;w=1.2;wdamp= (1/6)^(1/MaxIter);
%% initial swarm(genrate particle positions or configurations which r radial)
%preallocation
particle(1,np).position = [];particle(1,np).Vel = [];particle(1,np).Cost = [];particle(1,np).LBposition = [];particle(1,np).LBCost = [];particle(1,np).Vel = zeros(np,nvar);particle(1,np).Cost = zeros(1,nvar);particle(1,np).LBposition = zeros(np,nvar);particle(1,np).LBCost = zeros(1,nvar);GBposition = round(rand(1,nvar));   
% GBCostMat = zeros(1,MaxIter);
GBCost = inf;
% GBCostMat = [];
for iii = 1:np
   iii
    S='go';
    while(strcmp(S,'go'))==1
        b=round(rand(1,nvar));[r,loss]=isr1(b);
        if r==1
            S='stop';
            particle(iii).position=b;particle(iii).Vel = rand(1,nvar);particle(iii).Cost =loss;particle(iii).LBposition = particle(iii).position;particle(iii).LBCost = particle(iii).Cost;
            if particle(iii).LBCost < GBCost;
                GBPosition = particle(iii).LBposition;
                GBCost = particle(iii).LBCost;
            end
        end
    end
end