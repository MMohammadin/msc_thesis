function set_position=set_position(x,nbus)
switch  nbus;case 19;nvar=13;tie=3;case 33;nvar=35;tie=5;end
Sigv = 1./x;
rid=Sigv-rand(1,nvar);sortrid=sort(rid);
sortridd(1:nvar)=sortrid(tie);
set_position = (rid > sortridd);   %position in which number of 1 r set
end