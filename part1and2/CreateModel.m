function model=CreateModel()

    X=[14 42 50 7 36 87 96 50 11 21 36 14 81 39 11 59 78 4 89 11 100 51 2 59 7];
    Y=[63 52 93 83 25 100 6 4 10 43 93 82 71 24 74 9 81 0 46 36 14 52 14 81 83];

    n=numel(X);
    
    d=zeros(n,n);
    
    for i=1:n
        for j=i+1:n
            d(i,j)=sqrt((X(i)-X(j))^2+(Y(i)-Y(j))^2);
            d(j,i)=d(i,j);
        end
    end
    
    model.n=n;
    model.X=X;
    model.Y=Y;
    model.d=d;

end