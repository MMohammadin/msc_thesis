function set_position=set_position(x)
global nvar tie
Sigv = 1./(1+exp(-x));
rid=Sigv-rand(1,nvar);sortrid=sort(rid);
sortridd(1:nvar)=sortrid(tie);
set_position = (rid > sortridd);   %position in which number of 1 r set
end