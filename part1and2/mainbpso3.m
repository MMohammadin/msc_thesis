% tic
function [GBCostMat,GBposition]=mainbpso3(DG)
clc;
%% Problem Params
global nbus r nvar tie DG,
define_constants;
nbus=33;     % nbus=input('Enter a number of nodes of network');
% Particle : a structure which shows 
switch  nbus;case 19;nvar=13;tie=3;case 33;nvar=35;tie=5;end
%% Algorithm Parameters
np =50;MaxIt =150;C1 = 2;C2 = 4-C1;w=1.2;wdamp= (1/6)^(1/MaxIt);
%preallocation
particle(1,np).position = [];particle(1,np).Vel = [];particle(1,np).Cost = [];particle(1,np).LBposition = [];particle(1,np).LBCost = [];
particle(1,np).Vel = zeros(np,nvar);particle(1,np).Cost = zeros(1,nvar);particle(1,np).LBposition = zeros(np,nvar);particle(1,np).LBCost = zeros(1,nvar);
GBposition = round(rand(1,nvar));GBCostMat =zeros(1,MaxIt);GBCost = inf;
%% initial swarm(genrate particle positions or configurations which r radial)
for ii = 1:np
    %    iii
    S='go';
    while(strcmp(S,'go'))==1
        particle(ii).position=round(rand(1,nvar));%         Sig = 1./(1+exp(-particle(ii).position));%         rid=Sig-rand(1,nvar);sortrid=sort(rid); %         sortridd(1:nvar)=sortrid(tie);%         particle(ii).position = (rid > sortridd);
        particle(ii).position=set_position(particle(ii).position);
        loss=Mycost(particle(ii).position,DG);
        if r==1
            S='stop';
         particle(ii).Vel = rand(1,nvar);particle(ii).Cost =loss;particle(ii).LBposition = particle(ii).position;particle(ii).LBCost = particle(ii).Cost;
            if particle(ii).LBCost < GBCost;
                GBPosition = particle(ii).LBposition;
                GBCost = particle(ii).LBCost;
            end
        end
    end
end
%% Main Loop
GBCostMat(1,1)=203;
GBCostMat(1,2)=GBCost;
for Iter= 3:MaxIt    %         Iter
    %% Velocity Update            %   if toc<240
    for ii = 1:np
        particle(ii).Vel =w*particle(ii).Vel+C1*rand*(particle(ii).LBposition - particle(ii).position) + C2*rand*(GBposition - particle(ii).position);
        %% position update
        SigV = 1./(1+exp(-particle(ii).Vel));
        rid=SigV-rand(1,nvar);sortrid=sort(rid); 
        sortridd(1:nvar)=sortrid(tie);
        particle(ii).position = (rid > sortridd);       %         particle(ii).position = (SigV > rand(1,nvar));
       loss=Mycost(particle(ii).position,DG);particle(ii).Cost = loss;
        %% GB and LB Update
        if particle(ii).Cost < particle(ii).LBCost
            particle(ii).LBposition = particle(ii).position;
            particle(ii).LBCost = particle(ii).Cost;
            if particle(ii).LBCost < GBCost;
                GBposition = particle(ii).LBposition;
                GBCost = particle(ii).LBCost;
            end
        end
    end
    %% Some Plots
    w=w*wdamp;
    GBCostMat(1,Iter) = GBCost;    %        for u=1:50; n(u,1)=particle(u).Cost; end; n'    % [Iter GBCost];
  end
figure;
plot(GBCostMat,'LineWidth',0.5,'Color',[0 0 0]);
grid;xlabel('Iteration Number');ylabel('System Loss (kW)');title('33-Bus System-mbpso');
% gb=GBCost;
% toc

end