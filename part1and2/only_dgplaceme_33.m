%% this mfile gives u loss of baran network for various capacity of Dg 
%DG is in bus 17
clc;
clear ;
close all;
define_constants;
mpc=loadcase('case33');
i=0;
for DG=[0 .1 .15 .18 .20 .22 .24   .25  .26 .27 .28 .30]
% DG= .141
i=i+1; 
mpopt = mpoption('VERBOSE',0,'OUT_SYS_SUM',0,'OUT_BUS' ,0,'OUT_BRANCH',0,'ENFORCE_Q_LIMS',1);
mpc.bus(17,PD)=mpc.bus(17,PD)-DG;
results=runpf(mpc,mpopt);
loss=1000*sum(abs(abs(results.branch(:,14))-abs(results.branch(:,16))));
a(i,1)=DG;
a(i,2)=loss;
end
a;
x=a(:,1);
y=a(:,2);
plot(x,y,'-KO','MarkerFaceColor',[1 0.55 0]')
grid on
xlim([0 .3]);
ylim([135 280]);
xlabel('Capacity of DG in bus 17(MW)');
ylabel('System Loss (kW)');
title('Loss Reduction through Only DG Placement');

