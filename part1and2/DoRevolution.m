function emp=DoRevolution(emp)

%     global ProblemSettings; CostFunction=ProblemSettings.CostFunction;
%     nvar=ProblemSettings.nvar; VarSize=ProblemSettings.VarSize;
%     VarMin=ProblemSettings.VarMin; VarMax=ProblemSettings.VarMax;
%         tie=ProblemSettings.tie;
% 
%     global ICASettings; pRevolution=ICASettings.pRevolution;
%     mu=ICASettings.mu;
    
    
 global  CostFunction VarMin VarMax  nEmp pRevolution mu nvar tie


nmu=ceil(mu*nvar);
    
    sigma=0.1*(VarMax-VarMin);
    
    nEmp=numel(emp);
    for k=1:nEmp
        NewPos = 1 - emp(k).Imp.Position;
        jj=randsample(nvar,nmu)';
        NewImp=emp(k).Imp;
        NewImp.Position(jj)=NewPos(jj);
        %modified ica
%         NewImp.Position=double(NewImp.Position>=0.5); 
        Sigy = 1./(1+exp(-(NewImp.Position)));
        rid=Sigy-rand(1,nvar);sortrid=sort(rid); 
        sortridd(1:nvar)=sortrid(tie);
        NewImp.Position = (rid > sortridd);
%         [z,sol] =MyCost(country(i).Position);
        [NewImp.Cost ]=CostFunction(NewImp.Position);
%         NewImp.Sol
        if NewImp.Cost<emp(k).Imp.Cost
            emp(k).Imp = NewImp;
        end
        
        for i=1:emp(k).nCol
            if rand<=pRevolution
                NewPos = 1 - emp(k).Col(i).Position;
                jj=randsample(nvar,nmu)';
                emp(k).Col(i).Position(jj) = NewPos(jj);

                emp(k).Col(i).Position = max(emp(k).Col(i).Position,VarMin);
                emp(k).Col(i).Position = min(emp(k).Col(i).Position,VarMax);
 %modified ica
        Sigy = 1./(1+exp(-(emp(k).Col(i).Position)));
        rid=Sigy-rand(1,nvar);sortrid=sort(rid); 
        sortridd(1:nvar)=sortrid(tie);
        emp(k).Col(i).Position = (rid > sortridd);
                [emp(k).Col(i).Cost ]= CostFunction(emp(k).Col(i).Position);
% emp(k).Col(i).Sol
            end
        end
    end

end