function gb=ica(DG)
% tic
clc;
clear;
close all;
%% Problem Definition
 global GBCostMat nbus CostFunction nvar  VarMin VarMax  nPop ... 
        MaxIt nEmp alpha beta pRevolution mu zeta tie
nbus=33;
% nbus=input('Enter a number of buses of network');
switch nbus;case 19;nvar=13;tie=3;case 33;nvar=35;tie=5;end
CostFunction=@(xhat) Mycost(xhat);   % Cost Function
VarMin=.4166;                        % Lower Bound of Variables
VarMax=1;                            % Upper Bound of Variables
% VarSize=[1 nvar];                    % Decision Variables Matrix Size

%% ICA Parameters
MaxIt=2;          
nPop=3;nEmp=2;            
alpha=1;           
beta=1;             
pRevolution=0.2;
mu=0.05; zeta=0.1;           
% ShareSettings;
%% Initialization  % Initialize Empires
emp=CreateInitialEmpires();
% Array to Hold Best Cost Values
BestCost=zeros(MaxIt,1);
GBCost = inf;GBCostMat =zeros(1,MaxIt);
GBCostMat(1,1)=emp(1).Imp.Cost;
%% ICA Main Loop
    for it=2:MaxIt%     if toc <60
        emp=AssimilateColonies(emp);
    emp=DoRevolution(emp);
    emp=IntraEmpireCompetition(emp);
    emp=UpdateTotalCost(emp);
    emp=InterEmpireCompetition(emp);
    imp=[emp.Imp];
    [~, BestImpIndex]=min([imp.Cost]);
    BestSol=imp(BestImpIndex);%     BestCost(it)=BestSol.Cost;%  BestCost=BestSol.Cost;  
%     BestSol.
if BestSol.Cost < GBCost
 GBCost=BestSol.Cost;
end
    GBCostMat(1,it) = GBCost;
%     end% gb=mean(GBCostMat)
end
%% Results
gb=GBCostMat(it);
% figure;
% plot(GBCostMat,'LineWidth',0.5,'Color',[0 0 0]);
% xlabel('Iteration');
% ylabel('System Loss (kW)');
% title('33-Bus System-mica');


% save  mm GBCostMat
% toc
