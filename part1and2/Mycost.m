function [loss] =Mycost(position,DG) %y=b x= staus of swich =swich    y=13 or 35  % ,sol
global nbus r DG

define_constants;
Vmin=0.9;Vmax=1.1;
r=0;      % feasible=0;  % b=position;   % sol.r=r;
% DG2_Rating=8;QMax_DG2=4;QMin_DG2=-4;
% switch  nbus;case 19;loss=131;%cs=10;
%     case 33;
loss=203;
% end     
%     switch  nbus
%         case 19
%             x=[1 1 1 b(1:5) 1 1 b(6:7) 1 b(8:9) 1 1 b(10:13)];A=[0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;1 0 1  0 0 0 0 1 0 0 0 0 0 1 0 0 0 0 0;0 1 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;0 0 1 0 b(1,1) 0 0 0 0 0 0 0 0 0 0 0 0 0 0;0 0 0 b(1,1) 0 b(1,2) b(1,3) 0 0 0 0 0 0 0 0 0 0 0 0;0 0 0 0 b(1,2) 0 0 0 0 0 0 0 0 0 0 0 0 0 b(1,13);0 0 0 0 b(1,3) 0 0 0 0 b(1,4) 0 0 0 0 0 0 0 0 0;0 1 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 1 0 0 b(1,6) 0  0 0 0 0 0 0 0;0 0 0 0 0 0 b(1,4) 0 0 0 b(1,5) 0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0 1 b(1,5) 0 b(1,7) 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0 0 0 b(1,7) 0 1 0 0 0 0 0 0;0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0;0 1 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0;0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 b(1,10) 0;0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 b(1,9) b(1,11) 0;0 0 0 0 0 0 0 0 0 0 0 b(1,8) 0 0 0 b(1,9) 0 0 0;0 0 0 0 0 0 0 0 0 0 0 0 0 0 b(1,10) b(1,11) 0 0 b(1,12);0 0 0 0 0 b(1,13) 0 0 0 0 0 0 0 0 0 0 0 b(1,12) 0];
            
%         case 33
            branch=[1 position(1:9) 1 position(10:35)];bt=vector33(branch);A=CreateMatrixFromVector(bt);
%     end
    L=logical((eye(nbus)+A)^nbus);
    q=1-mean(L(:));
    if q==0;   %it means that graph is connected and doesnt have isolated bus
        for ii=1:nbus
            for j=nbus:-1:1
                if histc(A(j,:),1)==1 && histc(A(j,:),0)==(nbus-1)
                    for k=1:nbus
                        A(j,k)=0;
                        A(k,j)=0;
                    end
                end
            end
        end
        ch4=sum((sum(A==zeros(nbus,nbus))));%     ch4=sum((sum(ch2)));%ch4=361 shows that the structure dose not have any loop
        if  ch4==(nbus^2) % if true the structure is radial
%             switch  nbus;case 19;Case=loadcase('case1');
%                 case 33;
                    mpc=loadcase('case33');
%         end%             gen(2,2)=DG2_Rating;gen(2,4)=QMax_DG2;gen(2,5)=QMin_DG2;
            mpopt = mpoption('VERBOSE',0,'OUT_SYS_SUM',0,'OUT_BUS' ,0,'OUT_BRANCH',0,'ENFORCE_Q_LIMS',1);
            r=1;
            mpc.branch(:,11)=(branch)';
%             mpc.bus(:,PD)=1.5* mpc.bus(:,PD);
PDofbus17=mpc.bus(17,PD);
PDofbus17=PDofbus17-DG;
mpc.bus(17,PD)=PDofbus17;
            results=runpf(mpc,mpopt);
            Vm=results.bus(:, VM);vmin_all=min(Vm);vmax_all=max(Vm);
            if (vmin_all<Vmin) || (vmax_all>Vmax)
                loss=203;
            else  %             sol.r=r;%             sol.feasible=feasible+1;
           loss=1000*sum(abs(abs(results.branch(:,14))-abs(results.branch(:,16))));
            end           %         if loss <200; loss=loss;  end
% %         else;            loss=loss+1000;
        end
% %     else;      loss=loss+50000*q;
    end   % else%     z=z+50000*abs(cs-t(2)) ;
end
% end
% function [z sol]=MyCost(position,model)
% x=double(position>=0.5);
%     d=model.d;
% A=CreateMatrixFromVector(x);
% q=CalcDisconnectivity(A);
% AD=A.*d;
% alpha=5*sum(d(:));
% SumAD=sum(AD(:));
% z=SumAD+alpha*q;
% sol.A=A;
% sol.q=q;
% sol.SumAD=SumAD;
% sol.z=z;
% sol.IsFeasible=(q==0);
% end

%% modified ica
% y=double(position>=0.5); %y=b
%   %% modified ica
% Sigy = 1./(1+exp(-y));
%         rid=Sigy-rand(1,nvar);sortrid=sort(rid); 
%         sortridd(1:nvar)=sortrid(tie);
%         y = (rid > sortridd);