function emp=CreateInitialEmpires()
% global ProblemSettings;
% global ICASettings;
 global  CostFunction nvar  VarMin VarMax  nPop nEmp alpha   r tie

 nCol=nPop-nEmp;

 % CostFunction=ProblemSettings.CostFunction;
% nvar=ProblemSettings.nvar; VarSize=ProblemSettings.VarSize;
% VarMin=ProblemSettings.VarMin; VarMax=ProblemSettings.VarMax;
% 
% nPop=ICASettings.nPop; nEmp=ICASettings.nEmp; nCol=nPop-nEmp;
% alpha=ICASettings.alpha;

empty_country.Position=zeros(1,nvar);
% empty_country.Position=[];
empty_country.Cost=[];
% empty_country.Sol=[];

country=repmat(empty_country,nPop,1);

for i=1:nPop
    S='go';
    while(strcmp(S,'go'))==1
        country(i).Position=unifrnd(VarMin,VarMax,1,nvar);
       country(i).Position=double(country(i).Position>=0.5);
%y=b
  %% modified ica
      
  Sig = 1./(1+exp(-(country(i).Position)));
        rid=Sig-rand(1,nvar);sortrid=sort(rid); 
        sortridd(1:nvar)=sortrid(tie);
        country(i).Position = (rid > sortridd);r=0;
      CostFunction(country(i).Position);
%         r=sol.r;
        if r==1
            S='stop';
           [country(i).Cost ]=CostFunction(country(i).Position);
%             country(i).Sol
        end
    end
end

costs=[country.Cost];
[~, SortOrder]=sort(costs);
country=country(SortOrder);
imp=country(1:nEmp);
col=country(nEmp+1:end);

empty_empire.Imp=[];
empty_empire.Col=repmat(empty_country,0,1);
empty_empire.nCol=0;
empty_empire.TotalCost=[];

emp=repmat(empty_empire,nEmp,1);

% Assign Imperialists
for k=1:nEmp
    emp(k).Imp=imp(k);
end

% Assign Colonies
P=exp(-alpha*[imp.Cost]/max([imp.Cost]));
P=P/sum(P);
P=P';
% sus
k=sus(P,nCol);
ii=1;
for j=1:nCol
    emp(k(ii)).Col=[emp(k(ii)).Col
        col(j)];
     emp(k(ii)).nCol=(emp(k(ii)).nCol)+1;
     ii=ii+1;
end
    
% for j=1:nCol
     
%     k=RouletteWheelSelection(P);
     %     emp(k).Col=[emp(k).Col
%         col(j)];
    %     emp(k).nCol=emp(k).nCol+1;
% end

emp=UpdateTotalCost(emp);

end